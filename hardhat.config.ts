import { HardhatUserConfig } from "hardhat/config";
import "hardhat-deploy";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import "hardhat-contract-sizer";
import "@nomiclabs/hardhat-solhint";
import "hardhat-prettier";
import "hardhat-watcher";
import "solidity-coverage";
import "hardhat-docgen";
import("dotenv/config");
import { mnemonic, networkConfig } from "./config";

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.8.9",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  namedAccounts: {
    deployer: 0,
    tokenOwner: 1,
    registry: 2,
    receiver: 3,
  },
  networks: {
    hardhat: {
      accounts: {
        mnemonic: mnemonic,
      },
      forking: {
        url: networkConfig.hardhat.url,
      },
    },
  },
  mocha: {
    timeout: 60000,
  },
  watcher: {
    test: {
      // key is the name for the watcherTask
      tasks: ["test"],
      files: ["./test/**/*"],
      verbose: true,
    },
  },
  docgen: {
    path: "./docs",
    clear: true,
    runOnCompile: true,
  },
};

export default config;
