import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";
import {
  SPENDIEVOL_INITIAL_SUPPLY,
  developmentChains,
  founders as configuredFounders,
} from "../config";
import { create as createIPFS } from "ipfs-http-client";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { log } = deployments;

  const { deployer, tokenOwner } = await getNamedAccounts();

  const spendieGov = await ethers.getContract("SpendieGov");
  const treasury = await ethers.getContract("Treasury");
  const spendieVol = await ethers.getContract("SpendieVol");

  const founders = [];

  if (developmentChains.includes(network.name)) {
    founders.push(deployer);
    founders.push(tokenOwner);
  } else {
    founders.push(...configuredFounders);
  }

  log("Airdropping to founders...", founders);

  for (var i = 0; i < founders.length; i++) {
    let image = await addFileToIPFS(i + 1);

    if (image) {
      log("NFT written to IPFS at", image.toString());

      let metadata = await addMetadataToIPFS(image.toString(), i + 1);

      log("Metadata available on IPFS at", metadata.toString());

      await spendieGov.safeMint(founders[i], metadata.toString());
    }
  }

  log("Funding timelock with SpendieVol for Liquidity Pools...");

  await spendieVol.mint(treasury.address, SPENDIEVOL_INITIAL_SUPPLY);
};

const addFileToIPFS = async (count: number) => {
  const fs = require("fs");

  const content = fs.readFileSync("./images/nfts/" + count + ".jpg");
  const ipfs = createIPFS();

  const cid = await ipfs.add(
    { path: "spendiegov.jpg", content: content },
    {
      cidVersion: 1,
      wrapWithDirectory: true,
    }
  );

  return await ipfs.pin.add(cid.cid);
};

const addMetadataToIPFS = async (fileHash: string, count: number) => {
  const ipfs = createIPFS();

  const metadata = {
    name: "SpendieGov NFT #" + count,
    description:
      "This SpendieGov token entitles the holder to a single vote in the SpendieDAO.",
    image: fileHash,
  };

  const cid = await ipfs.add(
    { path: "metadata.json", content: JSON.stringify(metadata) },
    { cidVersion: 1, wrapWithDirectory: true }
  );

  return await ipfs.pin.add(cid.cid);
};

export default func;
func.tags = ["setup", "all"];
