import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";
import "dotenv/config";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { log } = deployments;

  const { deployer } = await getNamedAccounts();

  const spendieGov = await ethers.getContract("SpendieGov");
  const governor = await ethers.getContract("SpendieGovernor");
  const treasury = await ethers.getContract("Treasury");
  const spendieVol = await ethers.getContract("SpendieVol");
  const redemption = await ethers.getContract("Redemption");

  log("Changing access control to treasury...");

  await treasury.grantRole(await treasury.PROPOSER_ROLE(), governor.address);
  await treasury.grantRole(
    await treasury.EXECUTOR_ROLE(),
    "0x0000000000000000000000000000000000000000"
  );
  await treasury.revokeRole(await treasury.TIMELOCK_ADMIN_ROLE(), deployer);

  // give treasury full access to gov token.
  await spendieGov.grantRole(
    await spendieGov.DEFAULT_ADMIN_ROLE(),
    treasury.address
  );
  await spendieGov.grantRole(await spendieGov.MINTER_ROLE(), treasury.address);
  await spendieGov.grantRole(await spendieGov.PAUSER_ROLE(), treasury.address);

  // remove deployer once roles have been set up.
  await spendieGov.revokeRole(await spendieGov.MINTER_ROLE(), deployer);
  await spendieGov.revokeRole(await spendieGov.PAUSER_ROLE(), deployer);
  await spendieGov.revokeRole(await spendieGov.DEFAULT_ADMIN_ROLE(), deployer);

  // give treasury full access to vol token.
  await spendieVol.grantRole(
    await spendieVol.DEFAULT_ADMIN_ROLE(),
    treasury.address
  );
  await spendieVol.grantRole(await spendieVol.MINTER_ROLE(), treasury.address);
  await spendieVol.grantRole(await spendieVol.PAUSER_ROLE(), treasury.address);

  // give redemption minting access on spVol
  await spendieVol.grantRole(
    await spendieVol.MINTER_ROLE(),
    redemption.address
  );

  // remove deployer once roles have been set up.
  await treasury.revokeRole(await treasury.DEFAULT_ADMIN_ROLE(), deployer);

  await redemption.transferOwnership(treasury.address);
};

export default func;
func.tags = ["setup", "all"];
