// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";

import "./StableToken.sol";
import "./Treasury.sol";

import "hardhat/console.sol";

import {Create2} from "@openzeppelin/contracts/utils/Create2.sol";

/**
 * @title A basket of stable tokens.
 * @author hayden@spendie.io
 * @notice Manages a basket of stable tokens. Tokens can be added to the basket as required.
 */
contract Basket is Ownable {
    address[] private _tokens;
    mapping(address => address) private _tokenPriceFeeds;

    /**
     * Gets the number of stable tokens in the basket.
     * @return The number of stable tokens in the basket.
     */
    function getStableTokenCount() external view returns (uint256) {
        return _getStableTokenCount();
    }

    function _getStableTokenCount() internal view returns (uint256) {
        return _tokens.length;
    }

    /**
     * Gets the address of a stable token at the index provided.
     * @param index The index of the stable token.
     * @return The address of the stable token at the provided index.
     */
    function getStableToken(uint256 index) external view returns (address) {
        return _getStableToken(index);
    }

    function _getStableToken(uint256 index) internal view returns (address) {
        return _tokens[index];
    }

    /**
     * Gets the address of the Chainlink price feed associated with the token.
     * @param token The address of a stable token in the basket.
     * @return The address of the price feed associated with the token.
     */
    function getTokenPriceFeed(address token) external view returns (address) {
        return _getTokenPriceFeed(token);
    }

    function _getTokenPriceFeed(address token) internal view returns (address) {
        return _tokenPriceFeeds[token];
    }

    /**
     * Adds a stable token to the basket.
     * @param name The token's name.
     * @param symbol The token's symbol.
     * @param priceFeed The address of a chainlink price aggregator.
     * @param initialSupply The initial supply to mint.
     * @return The address of the new stable token.
     */
    function addStableToken(
        string memory name,
        string memory symbol,
        address priceFeed,
        uint256 initialSupply
    ) external onlyOwner returns (address) {
        address token = _addStableToken(name, symbol, priceFeed, initialSupply);

        emit StableTokenDeployed(token, name, symbol, priceFeed, initialSupply);

        return token;
    }

    function _addStableToken(
        string memory name,
        string memory symbol,
        address priceFeed,
        uint256 initialSupply
    ) internal returns (address) {
        Treasury treasury = Treasury(payable(owner()));

        // set up the token
        StableToken stableToken =
            new StableToken{salt: keccak256(abi.encodePacked(name, symbol))}(
                name,
                symbol
            );

        stableToken.grantRole(stableToken.PAUSER_ROLE(), owner());
        stableToken.grantRole(stableToken.MINTER_ROLE(), owner());
        stableToken.grantRole(stableToken.DEFAULT_ADMIN_ROLE(), owner());

        _tokens.push(address(stableToken));
        _tokenPriceFeeds[address(stableToken)] = priceFeed;

        stableToken.mint(address(treasury), initialSupply);

        // remove access to Basket once everything is done.
        stableToken.revokeRole(stableToken.PAUSER_ROLE(), address(this));
        stableToken.revokeRole(stableToken.MINTER_ROLE(), address(this));
        stableToken.revokeRole(stableToken.DEFAULT_ADMIN_ROLE(), address(this));

        return address(stableToken);
    }

    event StableTokenDeployed(
        address indexed token,
        string name,
        string symbol,
        address priceFeed,
        uint256 initialSupply
    );
}
