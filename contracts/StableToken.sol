// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Pausable.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";

/**
 * @title A stable token.
 * @author hayden@spendie.io
 * @notice StableToken is deployed when a new token is added to the Treasury's
 * stable token basket.
 */
contract StableToken is
    ERC20Permit,
    AccessControlEnumerable,
    ERC20Burnable,
    ERC20Pausable
{
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");

    uint8 private immutable _decimals;

    /**
     * Initializes the stable token.
     * @dev Admin, pauser and minter roles are assigned to the `msg.sender`.
     * @param name The name to use for the token.
     * @param symbol The symbol to use for the token.
     */
    constructor(string memory name, string memory symbol)
        ERC20Permit(name)
        ERC20(name, symbol)
    {
        _decimals = 8;

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(PAUSER_ROLE, msg.sender);
        _grantRole(MINTER_ROLE, msg.sender);
    }

    /**
     * inheritdoc ERC20
     */
    function decimals() public view virtual override returns (uint8) {
        return _decimals;
    }

    /**
     * Mints new stable tokens.
     * @param to The address to send the stable tokens to.
     * @param amount The amount of stable tokens to mint.
     */
    function mint(address to, uint256 amount)
        public
        virtual
        onlyRole(MINTER_ROLE)
    {
        _mint(to, amount);
    }

    /**
     * Pauses the contract. Must have PAUSER_ROLE.
     */
    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    /**
     * Unpauses the contract. Must have PAUSER_ROLE.
     */
    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override(ERC20, ERC20Pausable) {
        super._beforeTokenTransfer(from, to, amount);
    }
}
