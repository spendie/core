// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

import "./StableToken.sol";
import "./Treasury.sol";
import "./StableTokenETHPairLibrary.sol";

/**
 * @title Issuance of stable tokens for ETH.
 * @author hayden@spendie.io
 * @notice Issues stable tokens for ETH. ETH is the generic term used to describe the native token of an EVM compatible blockchain (e.g. matic, avax, etc).
 */
contract Issuance is Ownable {
    using StableTokenETHPairLibrary for AggregatorV3Interface;

    /// The _treasury.
    address payable private immutable _treasury;

    /// The base USD/ETH aggregator.
    address private immutable _aggregatorUSDETH;

    /// The issuance fee as a fraction of 100. Default is 2%.
    uint8 public fee = 2;

    /**
     * Initializes issuance.
     * @param treasury The address of the parent treasury.
     */
    constructor(address payable treasury, address aggregatorUSDETH) {
        _treasury = treasury;
        _aggregatorUSDETH = aggregatorUSDETH;
    }

    /**
     * Sets the issuance fee.
     * @param newFee The new fee.
     */
    function setFee(uint8 newFee) external onlyOwner {
        require(newFee > 0 && newFee < 100, "Issuance/fee-out-of-bounds");

        uint8 oldFee = fee;
        fee = newFee;

        emit FeeSet(newFee, oldFee);
    }

    /**
     * Issues the maximum amount of stable tokens for an exact amount of ETH.
     * @param token The address of the token being issued.
     * @param minAmountOut The minimum amount of tokens expected.
     * @param deadline The maximum amount of time allowed for executing the issuance.
     */
    function issueTokensForExactETH(
        address token,
        uint256 minAmountOut,
        uint256 deadline
    ) external payable {
        require(deadline >= block.timestamp, "Issuance/deadline-expired");

        require(address(token) != address(0), "Issuance/stabletoken-not-set");

        address priceFeed =
            Treasury(_treasury).basket().getTokenPriceFeed(token);

        require(address(priceFeed) != address(0), "Issuance/pricefeed-not-set");

        uint256 amountIn = msg.value;

        uint256 ethInMinusFee = amountIn - (amountIn * fee) / 100; // deduct "fee" of x%.

        uint256 amountOut =
            uint256(
                AggregatorV3Interface(priceFeed).getETHToTokenAmount(
                    _aggregatorUSDETH,
                    int256(ethInMinusFee)
                )
            );

        require(amountOut >= minAmountOut, "Issuance/minimum-amount-not-met");

        (bool success, ) = _treasury.call{value: msg.value}("");
        require(success, "Issuance/cannot-deposit-ether");

        StableToken(token).mint(_msgSender(), amountOut);

        emit Issued(token, amountIn, (amountIn * fee) / 100, amountOut);
    }

    /**
     * Issues the exact amount of stable tokens for the least amount of ETH.
     * @param token The address of the token being issued.
     * @param exactAmountOut The exact amount of tokens expected.
     * @param deadline The maximum amount of time allowed for executing the issuance.
     */
    function issueExactTokensForETH(
        address token,
        uint256 exactAmountOut,
        uint256 deadline
    ) external payable {
        require(deadline >= block.timestamp, "Issuance/deadline-expired");

        require(address(token) != address(0), "Issuance/stabletoken-not-set");

        address priceFeed =
            Treasury(payable(_treasury)).basket().getTokenPriceFeed(token);

        require(address(priceFeed) != address(0), "Issuance/pricefeed-not-set");

        uint256 ethIn =
            uint256(
                AggregatorV3Interface(priceFeed).getTokenToETHAmount(
                    _aggregatorUSDETH,
                    int256(exactAmountOut)
                )
            );

        uint256 ethInPlusFee = ethIn + (ethIn * fee) / 100; // include "fee" of x%.

        require(ethInPlusFee <= msg.value, "Issuance/not-enough-ether");

        (bool success, ) = _treasury.call{value: ethInPlusFee}("");
        require(success, "Issuance/cannot-deposit-ether");

        // refund excess ETH.
        (success, ) = address(msg.sender).call{value: msg.value - ethInPlusFee}(
            ""
        );
        require(success, "Issuance/cannot-refund-ether");

        StableToken(token).mint(_msgSender(), exactAmountOut);

        emit Issued(token, ethInPlusFee, (ethIn * fee) / 100, exactAmountOut);
    }

    event Issued(
        address indexed token,
        uint256 amountIn,
        uint256 fees,
        uint256 amountOut
    );

    event FeeSet(uint8 newFee, uint8 oldFee);
}
