// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";

import "@chainlink/contracts/src/v0.8/interfaces/KeeperCompatibleInterface.sol";

import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@uniswap/lib/contracts/libraries/FixedPoint.sol";
import "@uniswap/v2-periphery/contracts/libraries/UniswapV2OracleLibrary.sol";

import "./UniswapV2Library.sol";
import "./ISlidingWindowOracle.sol";

/**
 * @title A sliding window oracle for Uniswap.
 * @author hayden@spendie.io
 * @notice A Sliding window oracle that uses observations collected over a
 * window to provide moving price averages in the past `windowSize` with a
 * precision of `windowSize / granularity`.
 * @dev This is a singleton oracle and only needs to be deployed once per
 * desired parameters, which differs from the simple oracle which must be
 * deployed once per pair. This oracle is based on the Uniswap exmaple and is
 * available at https://github.com/Uniswap/v2-periphery/blob/master/contracts/examples/ExampleSlidingWindowOracle.sol.
 */
contract UniswapSlidingWindowOracle is
    KeeperCompatibleInterface,
    Pausable,
    AccessControlEnumerable,
    ISlidingWindowOracle
{
    using FixedPoint for *;

    struct Observation {
        uint256 timestamp;
        uint256 price0Cumulative;
        uint256 price1Cumulative;
    }

    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");

    /// @dev The address of the keeper registry.
    address public immutable keeperRegistryAddress;

    /// @dev The last time the keeper was run.
    uint256 public keeperTimestamp;

    /// @dev The Uniswap factory.
    address public immutable factory;

    /// @dev The desired amount of time over which the moving average should be computed, e.g. 24 hours.
    uint256 public immutable windowSize;

    /**
     * @dev The number of observations stored for each pair, i.e. how many price
     * observations are stored for the window.
     *
     * As granularity increases from 1, more frequent updates are needed,
     * but moving averages become more precise. Averages are computed over
     * intervals with sizes in the range:
     *
     *  [windowSize - (windowSize / granularity) * 2, windowSize]
     *
     * e.g. if the window size is 24 hours, and the granularity is 24, the
     * oracle will return the average price for the period:
     *
     *  [now - [22 hours, 24 hours], now]
     */
    uint8 public immutable granularity;

    /// @dev This is redundant with granularity and windowSize, but stored for gas savings & informational purposes.
    uint256 public immutable periodSize;

    /// @dev Mapping from pair address to a list of price observations of that pair
    mapping(address => Observation[]) public pairObservations;

    /**
     * @dev Initializes a Uniswap sliding window oracle.
     *
     * @param factory_ The address of the Uniswap factory.
     * @param windowSize_ The desired amount of time over which the moving
     * average should be computed, e.g. 24 hours.
     * @param granularity_ The number of observations stored for each pair,
     * i.e. how many price observations are stored for the window.
     * @param keeperRegistryAddress_ The address of the Upkeep registry.
     */
    constructor(
        address factory_,
        uint256 windowSize_,
        uint8 granularity_,
        address keeperRegistryAddress_
    ) {
        require(granularity_ > 1, "SlidingWindowOracle/granularity");
        require(
            (periodSize = windowSize_ / granularity_) * granularity_ ==
                windowSize_,
            "SlidingWindowOracle/window-not-evenly-divisible"
        );
        factory = factory_;
        windowSize = windowSize_;
        granularity = granularity_;

        keeperTimestamp = 0;
        keeperRegistryAddress = keeperRegistryAddress_;

        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(PAUSER_ROLE, _msgSender());
    }

    function _getInterval() internal view returns (uint256 interval) {
        return windowSize / granularity;
    }

    /**
     * @dev Retrieves the index of the observation corresponding to the given
     * timestamp.
     *
     * @param timestamp A timestamp to store the observation at.
     * @return index The index of the observation at timestamp.
     */
    function observationIndexOf(uint256 timestamp)
        public
        view
        returns (uint8 index)
    {
        uint256 epochPeriod = timestamp / periodSize;
        return uint8(epochPeriod % granularity);
    }

    /**
     * @dev Retrieves the observation from the oldest epoch (at the beginning
     * of the window) relative to the current time.
     *
     * @param pair The address of the pair to retrieve.
     * @return firstObservation The observation from the oldest epoch.
     */
    function getFirstObservationInWindow(address pair)
        private
        view
        returns (Observation storage firstObservation)
    {
        uint8 observationIndex = observationIndexOf(block.timestamp);
        // no overflow issue. if observationIndex + 1 overflows, result is still zero.
        uint8 firstObservationIndex = (observationIndex + 1) % granularity;
        firstObservation = pairObservations[pair][firstObservationIndex];
    }

    /**
     * Updates the cumulative price for the observation at the current
     * timestamp. each observation is updated at most once per epoch period.
     *
     * @param tokenA The first token of the pair.
     * @param tokenB The second token of the pair.
     */
    function _update(address tokenA, address tokenB) internal {
        address pair = UniswapV2Library.pairFor(factory, tokenA, tokenB);

        // populate the array with empty observations (first call only)
        for (uint256 i = pairObservations[pair].length; i < granularity; i++) {
            pairObservations[pair].push();
        }

        // get the observation for the current period
        uint8 observationIndex = observationIndexOf(block.timestamp);
        Observation storage observation =
            pairObservations[pair][observationIndex];

        // we only want to commit updates once per period (i.e. windowSize / granularity)
        uint256 timeElapsed = block.timestamp - observation.timestamp;
        if (timeElapsed > periodSize) {
            (uint256 price0Cumulative, uint256 price1Cumulative, ) =
                UniswapV2OracleLibrary.currentCumulativePrices(pair);
            observation.timestamp = block.timestamp;
            observation.price0Cumulative = price0Cumulative;
            observation.price1Cumulative = price1Cumulative;
        }
    }

    /**
     * @dev Computes the average price in terms of how much amount out is
     * received for the amount in given the cumulative prices of the start and
     * end of a period, and the length of the period.
     *
     * @param priceCumulativeStart The cumulative start price.
     * @param priceCumulativeEnd The cumulative end price.
     * @param timeElapsed The length of the average price period.
     * @param amountIn A given amount to convert.
     * @return amountOut The equivalent amount out.
     */
    function computeAmountOut(
        uint256 priceCumulativeStart,
        uint256 priceCumulativeEnd,
        uint256 timeElapsed,
        uint256 amountIn
    ) private pure returns (uint256 amountOut) {
        // overflow is desired.
        FixedPoint.uq112x112 memory priceAverage =
            FixedPoint.uq112x112(
                uint224(
                    (priceCumulativeEnd - priceCumulativeStart) / timeElapsed
                )
            );
        amountOut = priceAverage.mul(amountIn).decode144();
    }

    /// @notice Convert the amount of `amountIn` from `tokenIn` to `tokenOut`.
    /// @inheritdoc ISlidingWindowOracle
    function consult(
        address tokenIn,
        uint256 amountIn,
        address tokenOut
    ) external view override returns (uint256 amountOut) {
        address pair = UniswapV2Library.pairFor(factory, tokenIn, tokenOut);
        Observation storage firstObservation =
            getFirstObservationInWindow(pair);

        uint256 timeElapsed = block.timestamp - firstObservation.timestamp;
        require(
            timeElapsed <= windowSize,
            "SlidingWindowOracle/missing-historical-observation"
        );
        // should never happen.
        require(
            timeElapsed >= windowSize - periodSize * 2,
            "SlidingWindowOracle/unexpected-time-elapsed"
        );

        (uint256 price0Cumulative, uint256 price1Cumulative, ) =
            UniswapV2OracleLibrary.currentCumulativePrices(pair);
        (address token0, ) = UniswapV2Library.sortTokens(tokenIn, tokenOut);

        if (token0 == tokenIn) {
            return
                computeAmountOut(
                    firstObservation.price0Cumulative,
                    price0Cumulative,
                    timeElapsed,
                    amountIn
                );
        } else {
            return
                computeAmountOut(
                    firstObservation.price1Cumulative,
                    price1Cumulative,
                    timeElapsed,
                    amountIn
                );
        }
    }

    /**
     * @notice Pause the Uniswap sliding window oracle.
     * @dev Pauses the contract. Must have PAUSER_ROLE.
     */
    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    /**
     * @notice Unpause the Uniswap sliding window oracle.
     * @dev Unpauses the contract. Must have PAUSER_ROLE.
     */
    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }

    function checkUpkeep(
        bytes calldata /*checkData*/
    )
        public
        view
        override
        returns (
            bool upkeepNeeded,
            bytes memory /*checkData*/
        )
    {
        upkeepNeeded = (block.timestamp - keeperTimestamp) > _getInterval();
    }

    function performUpkeep(bytes calldata performData)
        external
        override
        onlyRegistry
        whenNotPaused
    {
        keeperTimestamp = block.timestamp;

        (address tokenA, address tokenB) =
            abi.decode(performData, (address, address));
        _update(tokenA, tokenB);
    }

    modifier onlyRegistry() {
        require(
            _msgSender() == keeperRegistryAddress,
            "UniswapSlidingWindowOracle/only-registry"
        );
        _;
    }
}
