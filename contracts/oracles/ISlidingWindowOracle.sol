// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

/**
 * @title A slding window oracle interface for providing a historical average price of one or more price pairs.
 * @author hayden@spendie.io
 * @notice Provides a common set of methods for interacting with sliding window oracles.
 * @dev This interface should be used when interacting with Spendie-based sliding window oracles.
 */
interface ISlidingWindowOracle {
    /**
     * Retrieves the amount out corresponding to the amount in for a given token using the moving average over the time. Range [now - [windowSize, windowSize - periodSize * 2], now]. Update must have been called for the bucket corresponding to timestamp `now - windowSize`.
     * @param tokenIn The address of the token in.
     * @param amountIn The amount of the token in.
     * @param tokenOut The address of the token out.
     * @return amountOut The equivalent amount of token out for token in.
     */
    function consult(
        address tokenIn,
        uint256 amountIn,
        address tokenOut
    ) external view returns (uint256 amountOut);
}
