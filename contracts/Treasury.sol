// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

import "./Basket.sol";
import "./StableToken.sol";

import "@openzeppelin/contracts/governance/TimelockController.sol";

/**
 * @title The Spendie treasury.
 * @author hayden@spendie.io
 * notice Treasury manages funds and executes governance proposals within the
 * Spendie ecosystem. Tresaury features a timelock to allow participants to
 * exit out of the system if they don't agree with a successful proposal.
 * @dev Timelock provides a grace period between the queuing and executing of a
 * successful proposal.
 */
contract Treasury is TimelockController {
    Basket public basket;

    /**
     * Initialize Tresaury.
     * @param minDelay The minimum amount of elapsed time between the queuing
     * of a successful proposal and its execution.
     * @param proposers The addresses of one or more proposers. Proposers can
     * submit new proposals.
     * @param executors The addresses of one or more executors. Executors can
     * execute successful proposals.
     */
    constructor(
        uint256 minDelay,
        address[] memory proposers,
        address[] memory executors
    ) TimelockController(minDelay, proposers, executors) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);

        basket = new Basket();
    }

    function withdraw(address receiver, uint256 amount) public payable {
        require(
            msg.sender == address(this),
            "Treasury/only-treasury-can-withdraw"
        );
        (bool success, ) = receiver.call{value: amount}("");
        require(success, "Issuance/cannot-send-ether");
    }
}
