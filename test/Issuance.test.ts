import { expect } from "chai";
import { ethers, waffle, deployments } from "hardhat";
import { BigNumber } from "ethers";

import { proposeSPAUD } from "./utils/proposals/proposeSPAUD";

import * as Issuance from "./utils/issuance";
import * as Slippage from "./utils/slippage";
import * as AUDETHPair from "./utils/pricefeeds/chainlink/rates/AUDETH";

import { getTwentyMinuteDeadline } from "./utils/deadline";
import { getTreasury, getBasket, getIssuance } from "./utils/contracts/core";
import { advanceBlockTimestamp } from "./utils/mining";
import { hour } from "./utils/constants";

describe("Issuance", () => {
  let treasury: any;
  let basket: any;
  let issuance: any;

  let provider: any;

  const initialSupply = ethers.utils.parseEther("1");

  before(async () => {
    provider = waffle.provider;
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);

    treasury = await getTreasury();
    basket = await getBasket();
    issuance = await getIssuance();

    const tx = {
      to: treasury.address,
      value: ethers.utils.parseEther("100"),
    };

    await provider.getSigner().sendTransaction(tx);

    await proposeSPAUD(initialSupply);
  });

  describe("Issue stable tokens for exact ETH", () => {
    let amountETHIn: BigNumber;
    let amountAUDOut: BigNumber;
    let minAmountAUDOut: BigNumber;
    let options: { value: BigNumber };

    beforeEach(async () => {
      amountETHIn = ethers.utils.parseEther("1");
      amountAUDOut = await AUDETHPair.getETHToAUDAmount(amountETHIn);
      minAmountAUDOut = Issuance.getAmountMinusIssuanceFee(amountAUDOut);

      options = { value: amountETHIn };
    });

    it("should issue SpendieAUD for 1 ETH and send to user", async () => {
      const expectedAmountAUDOut = minAmountAUDOut;

      await issuance
        .connect(provider.getSigner())
        .issueTokensForExactETH(
          basket.getStableToken(0),
          minAmountAUDOut,
          getTwentyMinuteDeadline(),
          options
        );

      const spAUDAddress = await basket.getStableToken(0);
      const spAUD = new ethers.Contract(
        spAUDAddress,
        (await deployments.getArtifact("StableToken")).abi,
        provider
      );

      expect(
        await spAUD.balanceOf(provider.getSigner().getAddress())
      ).to.be.equal(expectedAmountAUDOut);
    });

    it("should emit the event Issued when issuing min amount out", async () => {
      const expectedAmountAUDOut = minAmountAUDOut;
      const expectedFees = Issuance.calculateIssuanceFee(amountETHIn);

      await expect(
        issuance
          .connect(provider.getSigner())
          .issueTokensForExactETH(
            basket.getStableToken(0),
            minAmountAUDOut,
            getTwentyMinuteDeadline(),
            options
          )
      )
        .to.emit(issuance, "Issued")
        .withArgs(
          await basket.getStableToken(0),
          amountETHIn,
          expectedFees,
          expectedAmountAUDOut
        );
    });

    it("should revert when min amount out not met", async () => {
      // minus 1 gwei
      options.value = options.value.sub(ethers.utils.parseUnits("1", 10));

      await expect(
        issuance
          .connect(provider.getSigner())
          .issueTokensForExactETH(
            basket.getStableToken(0),
            minAmountAUDOut,
            getTwentyMinuteDeadline(),
            options
          )
      ).to.be.revertedWith("Issuance/minimum-amount-not-met");
    });

    it("should revert when deadline expires", async () => {
      const deadline = await getTwentyMinuteDeadline();
      await advanceBlockTimestamp(provider, hour); // advance 1 hour to expire.

      await expect(
        issuance
          .connect(provider.getSigner())
          .issueTokensForExactETH(
            basket.getStableToken(0),
            minAmountAUDOut,
            deadline,
            options
          )
      ).to.be.revertedWith("Issuance/deadline-expired");
    });

    it("should issue when price slippage of 1% is allowed", async () => {
      // Emulate 0.5% drop.
      options.value = options.value.sub(options.value.mul("5").div("1000"));

      const minAmountAUDOutMinusSlippage =
        Slippage.getAmountMinusSlippage(minAmountAUDOut);

      // this is the adjusted price from above minus 1 gwei. This will be the
      // price that the contract will seek.
      const expectedAmountAUDOut = Issuance.getAmountMinusIssuanceFee(
        await AUDETHPair.getETHToAUDAmount(options.value)
      );

      await issuance
        .connect(provider.getSigner())
        .issueTokensForExactETH(
          basket.getStableToken(0),
          minAmountAUDOutMinusSlippage,
          getTwentyMinuteDeadline(),
          options
        );

      const spAUDAddress = await basket.getStableToken(0);
      const spAUD = new ethers.Contract(
        spAUDAddress,
        (await deployments.getArtifact("StableToken")).abi,
        provider
      );

      expect(
        await spAUD.balanceOf(provider.getSigner().getAddress())
      ).to.be.equal(expectedAmountAUDOut);
    });

    it("should revert when price below slippage", async () => {
      // Emulate 2% drop in eth against AUD.
      options.value = options.value.sub(options.value.mul("2").div("100"));

      const minAmountAUDOutMinusSlippage =
        Slippage.getAmountMinusSlippage(minAmountAUDOut);

      await expect(
        issuance
          .connect(provider.getSigner())
          .issueTokensForExactETH(
            basket.getStableToken(0),
            minAmountAUDOutMinusSlippage,
            getTwentyMinuteDeadline(),
            options
          )
      ).to.be.revertedWith("Issuance/minimum-amount-not-met");
    });
  });

  describe("Issue exact stable tokens for ETH", () => {
    let exactAmountAUDOut: BigNumber;
    let amountETHIn: BigNumber;
    let amountETHInPlusFee: BigNumber;

    let options: { value: BigNumber };

    beforeEach(async () => {
      exactAmountAUDOut = ethers.utils.parseUnits("1", 8);

      amountETHIn = await AUDETHPair.getAUDToETHAmount(exactAmountAUDOut);
      amountETHInPlusFee = Issuance.getAmountPlusIssuanceFee(amountETHIn);

      options = { value: amountETHInPlusFee };
    });

    it("should issue exactly 1 SpendieAUD", async () => {
      const expectedAmountAUDOut = exactAmountAUDOut;

      await issuance
        .connect(provider.getSigner())
        .issueExactTokensForETH(
          basket.getStableToken(0),
          exactAmountAUDOut,
          getTwentyMinuteDeadline(),
          options
        );

      const spAUDAddress = await basket.getStableToken(0);
      const spAUD = new ethers.Contract(
        spAUDAddress,
        (await deployments.getArtifact("StableToken")).abi,
        provider
      );

      expect(
        await spAUD.balanceOf(provider.getSigner().getAddress())
      ).to.be.equal(expectedAmountAUDOut);
    });

    it("should emit the event Issued when issuing exact amount out", async () => {
      const expectedFees = Issuance.calculateIssuanceFee(amountETHIn);

      await expect(
        issuance
          .connect(provider.getSigner())
          .issueExactTokensForETH(
            basket.getStableToken(0),
            exactAmountAUDOut,
            getTwentyMinuteDeadline(),
            options
          )
      )
        .to.emit(issuance, "Issued")
        .withArgs(
          await basket.getStableToken(0),
          amountETHInPlusFee,
          expectedFees,
          exactAmountAUDOut
        );
    });

    it("should revert when not enough ETH in", async () => {
      // minus 1 gwei
      options.value = options.value.sub(ethers.utils.parseUnits("1", 10));

      await expect(
        issuance
          .connect(provider.getSigner())
          .issueExactTokensForETH(
            basket.getStableToken(0),
            exactAmountAUDOut,
            getTwentyMinuteDeadline(),
            options
          )
      ).to.be.revertedWith("Issuance/not-enough-ether");
    });

    it("should revert when deadline expires", async () => {
      const deadline = await getTwentyMinuteDeadline();
      await advanceBlockTimestamp(provider, hour); // advance 1 hour to expire.

      await expect(
        issuance
          .connect(provider.getSigner())
          .issueExactTokensForETH(
            basket.getStableToken(0),
            exactAmountAUDOut,
            deadline,
            options
          )
      ).to.be.revertedWith("Issuance/deadline-expired");
    });

    it("should refund excess ether when issuing exactly 1 SpendieAUD", async () => {
      const exactAmountAUDOut = ethers.utils.parseUnits("1", 8);

      const amountETHIn = await AUDETHPair.getAUDToETHAmount(exactAmountAUDOut);
      let amountETHInWithIssuanceFee =
        Issuance.getAmountPlusIssuanceFee(amountETHIn);
      let expectedETHBalance = await provider.getBalance(
        provider.getSigner().getAddress()
      );

      // get balance sub amount eth required then add another eth to tx for an overspend on issuance.
      expectedETHBalance = expectedETHBalance.sub(amountETHInWithIssuanceFee);
      amountETHInWithIssuanceFee = amountETHIn.add(
        ethers.utils.parseEther("1")
      );

      const options = { value: amountETHInWithIssuanceFee };

      const tx = await issuance
        .connect(provider.getSigner())
        .issueExactTokensForETH(
          basket.getStableToken(0),
          exactAmountAUDOut,
          getTwentyMinuteDeadline(),
          options
        );

      const receipt = await tx.wait();

      // expect some gas fees
      expectedETHBalance = expectedETHBalance.sub(
        receipt.effectiveGasPrice.mul(receipt.gasUsed)
      );

      // signer's account should be credited with extra 1 eth.
      expect(
        await provider.getBalance(provider.getSigner().getAddress())
      ).to.be.equal(expectedETHBalance);
    });
  });

  it("should transfer ether to treasury during issuance", async () => {
    const amountETHIn = ethers.utils.parseEther("1");
    const amountAUDOut = await AUDETHPair.getETHToAUDAmount(amountETHIn);
    const minAmountAUDOut = Issuance.getAmountMinusIssuanceFee(amountAUDOut);

    let treasuryETHBalance = await provider.getBalance(treasury.address);

    const options = { value: amountETHIn };

    await issuance
      .connect(provider.getSigner())
      .issueTokensForExactETH(
        basket.getStableToken(0),
        minAmountAUDOut,
        getTwentyMinuteDeadline(),
        options
      );

    expect(treasuryETHBalance.add(amountETHIn)).to.be.equal(
      await provider.getBalance(treasury.address)
    );
  });

  it("should not hold any ether in issuance after issue.", async () => {
    const amountETHIn = ethers.utils.parseEther("1");
    const amountAUDOut = await AUDETHPair.getETHToAUDAmount(amountETHIn);
    const minAmountAUDOut = Issuance.getAmountMinusIssuanceFee(amountAUDOut);

    const options = { value: amountETHIn };

    await issuance
      .connect(provider.getSigner())
      .issueTokensForExactETH(
        basket.getStableToken(0),
        minAmountAUDOut,
        getTwentyMinuteDeadline(),
        options
      );

    expect(await provider.getBalance(issuance.address)).to.be.equal(0);
  });

  it("should not issue if ether sent is zero", async () => {
    const amountETHIn = ethers.utils.parseEther("1");
    const amountAUDOut = await AUDETHPair.getETHToAUDAmount(amountETHIn);
    const minAmountAUDOut = Issuance.getAmountMinusIssuanceFee(amountAUDOut);

    const options = { value: 0 };

    await expect(
      issuance
        .connect(provider.getSigner())
        .issueTokensForExactETH(
          basket.getStableToken(0),
          minAmountAUDOut,
          getTwentyMinuteDeadline(),
          options
        )
    ).to.be.revertedWith("Issuance/minimum-amount-not-met");
  });
});
