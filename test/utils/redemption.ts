import { BigNumber } from "ethers";
import { REDEMPTION_FEE } from "./constants";

export function calculateRedemptionFee(amount: BigNumber): BigNumber {
  return amount.mul(REDEMPTION_FEE).div(100);
}

export const getAmountPlusRedemptionFee = (amount: BigNumber) => {
  return amount.add(calculateRedemptionFee(amount));
};

export const getAmountMinusRedemptionFee = (amount: BigNumber) => {
  return amount.sub(calculateRedemptionFee(amount));
};
