import { ethers, waffle } from "hardhat";
import { VoteType, hour } from "./constants";
import { getSpendieGov, getGovernor } from "./contracts/core";
import { BigNumber } from "ethers";
import { mineNBlocks, advanceBlockTimestamp } from "./mining";

export default async (
  targetAddress: string[],
  values: BigNumber[],
  callData: string[],
  description: string
) => {
  const [, addr1] = await ethers.getSigners();

  const provider = waffle.provider;
  const governor = await getGovernor();
  const spGov = await getSpendieGov();

  await spGov.delegate(await provider.getSigner().getAddress());
  await spGov.connect(addr1).delegate(addr1.getAddress());

  await governor.propose(targetAddress, values, callData, description);

  await mineNBlocks(provider, 1);

  const descriptionHash = ethers.utils.id(description);

  const proposalHash = await governor.hashProposal(
    targetAddress,
    values,
    callData,
    descriptionHash
  );

  await governor.castVote(proposalHash, VoteType.For);

  await governor.connect(addr1).castVote(proposalHash, VoteType.For);

  await mineNBlocks(provider, await governor.votingPeriod());

  await governor.queue(targetAddress, values, callData, descriptionHash);

  await advanceBlockTimestamp(provider, hour);
  await mineNBlocks(provider, 1);

  return await governor.execute(
    targetAddress,
    values,
    callData,
    descriptionHash
  );
};
