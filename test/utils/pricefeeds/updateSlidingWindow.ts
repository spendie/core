import { ethers } from "hardhat";
import { getUniswapV2Router02 } from "../contracts/periphery";
import { getSpendieVol } from "../contracts/core";

export default async () => {
  const oracle = await ethers.getContract("UniswapSlidingWindowOracle");
  const uniswapV2Router02 = await getUniswapV2Router02();
  const spVol = await getSpendieVol();

  const abiCoder = new ethers.utils.AbiCoder();

  const [, , registry] = await ethers.getSigners();

  await oracle
    .connect(registry)
    .performUpkeep(
      abiCoder.encode(
        ["address", "address"],
        [spVol.address, await uniswapV2Router02.WETH()]
      )
    );
};
