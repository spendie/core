import { ethers } from "hardhat";
import { getETHUSDAggregator } from "../pairs";

export const getETHToUSDRate = async () => {
  const aggregator = await getETHUSDAggregator();
  const roundData = await aggregator.latestRoundData();

  return roundData.answer;
};

export const getUSDToETHRate = async () => {
  const aggregator = await getETHUSDAggregator();

  const decimals = await aggregator.decimals();

  return ethers.utils
    .parseEther("1")
    .mul(ethers.utils.parseUnits("1", decimals))
    .div(await getETHToUSDRate());
};
