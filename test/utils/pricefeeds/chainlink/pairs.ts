/**
 * Convenience functions for retrieving Chainlink aggregator information.
 */
import { network } from "hardhat";
import { BigNumber } from "ethers";
import { getChainlinkAggregator } from "../../contracts/periphery";
import { networkConfig } from "../../../../config";

export const getAUDUSDAggregator = async () => {
  return await getChainlinkAggregator(
    networkConfig[network.name].chainlink.pairs.AUDUSD
  );
};

export const getETHUSDAggregator = async () => {
  return await getChainlinkAggregator(
    networkConfig[network.name].chainlink.pairs.ETHUSD
  );
};

/**
 * Calculates an exchange rate between two price pairs.
 *
 * Useful for calculating the exchange rate between currencies which have no
 * direct aggregator. For example, converting ETH to AUD would require
 * querying the price of ETH to USD then USD to AUD.
 */
export const derivePairRate = async (
  a: string,
  b: string
): Promise<BigNumber> => {
  const pairA = await getPairRate(a);
  const pairB = await getPairRate(b);

  const priceA = pairA.answer;
  const priceB = pairB.answer;
  const decimalsB = await getPairDecimals(b);

  return priceA.mul(BigNumber.from("10").pow(decimalsB)).div(priceB);
};

/**
 * Gets the latestRoundData from the aggregator specified by `address`.
 */
export const getPairRate = async (address: string): Promise<any> => {
  const aggregator = await getChainlinkAggregator(address);

  return await aggregator.latestRoundData();
};

/**
 * Gets the associated decimal places of the aggregator.
 */
export const getPairDecimals = async (address: string): Promise<BigNumber> => {
  const aggregator = await getChainlinkAggregator(address);

  return await aggregator.decimals();
};
