import { ethers } from "hardhat";
import testProposal from "../testProposal";
import { ZERO } from "../constants";
import { getSpendieVol, getGovernor } from "../../utils/contracts/core";
import { getOneDayDeadline } from "../../utils/deadline";

export async function proposeAddSPVolETHLiquidity(uniswapV2Router02: any) {
  const governor = await getGovernor();
  const spVol = await getSpendieVol();

  const callData = [];
  const values = [];
  const deadline = (await governor.votingDelay()).add(
    await getOneDayDeadline()
  );

  callData.push(
    spVol.interface.encodeFunctionData("approve", [
      uniswapV2Router02.address,
      ethers.utils.parseEther("1"),
    ])
  );

  values.push(ZERO);

  callData.push(
    uniswapV2Router02.interface.encodeFunctionData("addLiquidityETH", [
      spVol.address,
      ethers.utils.parseEther("1"),
      ethers.utils.parseEther("1"),
      ethers.utils.parseEther("1"),
      await governor.timelock(),
      deadline,
    ])
  );

  values.push(ethers.utils.parseEther("1"));

  try {
    await testProposal(
      [spVol.address, uniswapV2Router02.address],
      values,
      callData,
      "Add liquidity"
    );
  } catch (error) {
    console.log(error);
  }
}
