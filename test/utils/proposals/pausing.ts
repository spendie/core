import testProposal from "../testProposal";
import { ZERO } from "../constants";

export const proposePause = async (contract: any, walletAddress: any) => {
  const targetAddresses = [];
  const callDatas = [];
  const values = [];

  targetAddresses.push(contract.address);
  callDatas.push(
    contract.interface.encodeFunctionData("grantRole", [
      await contract.PAUSER_ROLE(),
      walletAddress,
    ])
  );
  values.push(ZERO);

  await testProposal(
    targetAddresses,
    values,
    callDatas,
    "Add emergency pauser to " + contract.address
  );
};
