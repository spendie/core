import { ethers, waffle, deployments } from "hardhat";
import { Contract } from "ethers";
import { UNISWAP_V2_ROUTER_02 } from "../../../config";

import ArtifactIUniswapV2Router02 from "@uniswap/v2-periphery/build/IUniswapV2Router02.json";
import ArtifactIUniswapV2Factory from "@uniswap/v2-core/build/IUniswapV2Factory.json";
import AggegatorV3InterfaceABI from "@chainlink/contracts/abi/v0.8/AggregatorV3Interface.json";

export async function getUniswapV2Router02() {
  const uniswapV2Router02Address = UNISWAP_V2_ROUTER_02;
  return new Contract(
    uniswapV2Router02Address,
    JSON.stringify(ArtifactIUniswapV2Router02.abi),
    waffle.provider.getSigner()
  );
}

export async function getUniswapV2Factory(provider: any) {
  const uniswapV2Router02 = await getUniswapV2Router02();
  const uniswapV2FactoryAddress = await uniswapV2Router02.factory();

  return new Contract(
    uniswapV2FactoryAddress,
    JSON.stringify(ArtifactIUniswapV2Factory.abi),
    provider.getSigner()
  );
}

export async function getUniswapV2Pair(
  provider: any,
  tokenIn: string,
  tokenOut: string
) {
  const uniswapV2Factory = await getUniswapV2Factory(provider);
  const uniswapV2PairAddress = await uniswapV2Factory.getPair(
    tokenIn,
    tokenOut
  );

  return new ethers.Contract(
    uniswapV2PairAddress,
    (await deployments.getArtifact("IUniswapV2Pair")).abi,
    provider.getSigner()
  );
}

export async function getChainlinkAggregator(address: string) {
  const aggregator = new Contract(
    address,
    AggegatorV3InterfaceABI,
    waffle.provider
  );

  return aggregator;
}
