import { expect } from "chai";
import { ethers, waffle, deployments, network } from "hardhat";
import { Contract } from "ethers";

// we need BigNumber.js for sqrt. ethers.utils.BigNumber doesn't have it.
import { BigNumber as BigNumberjs } from "bignumber.js";

import {
  getTreasury,
  getBasket,
  getSpendieVol,
  getSpendieGov,
} from "./utils/contracts/core";

import {
  getUniswapV2Router02,
  getUniswapV2Pair,
} from "./utils/contracts/periphery";

import testProposal from "./utils/testProposal";
import { proposeSPAUD } from "./utils/proposals/proposeSPAUD";
import { proposeAddSPVolETHLiquidity } from "./utils/proposals/proposeAddSPVolETHLiquidity";
import { proposePause } from "./utils/proposals/pausing";
import { getOneDayDeadline } from "./utils/deadline";
import { ZERO, SpendieAUD } from "./utils/constants";
import computeSPAUDAddress from "./utils/contracts/computeSPAUDAddress";

import { networkConfig } from "../config";

describe("Treasury", () => {
  const SPVOLTOTALSUPPLY = ethers.utils.parseEther("1000000");
  const SPGOVTOTALSUPPLY = 2;

  const SPINITIALSUPPLY = ethers.utils.parseUnits("1000000", 8);

  let treasury: any;
  let basket: any;
  let spVol: any;
  let spGov: any;

  let provider: any;

  before(async () => {
    provider = waffle.provider;
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);

    treasury = await getTreasury();

    basket = await getBasket();

    spVol = await getSpendieVol();

    spGov = await getSpendieGov();

    const tx = {
      to: treasury.address,
      value: ethers.utils.parseEther("100"),
    };

    await provider.getSigner().sendTransaction(tx);
  });

  it("shoud have an initial supply of 10 million Spendie Vol", async () => {
    expect(await spVol.totalSupply()).to.be.equal(SPVOLTOTALSUPPLY);
  });

  it("should have an initial supply of 2 SpendieGov NFTs", async () => {
    expect(await spGov.totalSupply()).to.be.equal(SPGOVTOTALSUPPLY);
  });

  it("should deploy a basket on initialization", async () => {
    expect(await basket.getStableTokenCount()).to.be.equal(0);
  });

  describe("stable tokens", () => {
    it("should deploy a SpendieAUD stable token", async () => {
      await proposeSPAUD(SPINITIALSUPPLY);

      const spAUDAddress = await basket.getStableToken(0);

      const spAUD = new ethers.Contract(
        spAUDAddress,
        (await deployments.getArtifact("StableToken")).abi,
        provider.getSigner()
      );

      const totalSupply = await spAUD.totalSupply();

      expect(totalSupply).to.be.equal(SPINITIALSUPPLY);
    });

    it("should not deploy a stable token directly", async () => {
      await expect(
        basket.addStableToken(
          SpendieAUD.symbol,
          "Spendie AUD",
          networkConfig[network.name].chainlink.pairs.AUDUSD,
          ethers.utils.parseUnits("1", 8)
        )
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("should emit an event when a stable token is created", async () => {
      await expect(proposeSPAUD(SPINITIALSUPPLY))
        .to.emit(basket, "StableTokenDeployed")
        .withArgs(
          await computeSPAUDAddress(),
          SpendieAUD.name,
          SpendieAUD.symbol,
          networkConfig[network.name].chainlink.pairs.AUDUSD,
          SPINITIALSUPPLY
        );
    });
  });

  describe("liquidity pools", () => {
    let uniswapV2Router02: any;
    let uniswapV2Pair: any;
    let liquidity: any;

    let deadline: Number;

    beforeEach(async () => {
      deadline = await getOneDayDeadline();

      uniswapV2Router02 = await getUniswapV2Router02();

      await proposeAddSPVolETHLiquidity(uniswapV2Router02);

      uniswapV2Pair = await getUniswapV2Pair(
        provider,
        spVol.address,
        uniswapV2Router02.WETH()
      );

      liquidity = await uniswapV2Pair.balanceOf(treasury.address);
    });

    it("should create SPVOL/ETH LP pair", async () => {
      const MINIMUM_LIQUIDITY = 10 ** 3;
      const amount = new BigNumberjs(ethers.utils.parseEther("1").toString());
      const expected = amount.times(amount).sqrt().minus(MINIMUM_LIQUIDITY);

      expect(liquidity).to.be.equal(expected.toFixed(0));
    });

    it("should withdraw 50% of the SPVOL/ETH LP pair", async () => {
      const spVol = await getSpendieVol();

      const withdrawLiquidity = liquidity.div(2);

      const [reserveA, reserveB] = await uniswapV2Pair.getReserves();

      const decimals = await uniswapV2Pair.decimals();
      const div = ethers.utils.parseUnits("10", decimals);

      const callData = [];
      const values = [ZERO, ZERO];

      callData.push(
        uniswapV2Pair.interface.encodeFunctionData("approve", [
          uniswapV2Router02.address,
          withdrawLiquidity,
        ])
      );

      callData.push(
        uniswapV2Router02.interface.encodeFunctionData("removeLiquidityETH", [
          spVol.address,
          withdrawLiquidity,
          reserveA.mul(withdrawLiquidity).div(div),
          reserveB.mul(withdrawLiquidity).div(div),
          await treasury.address,
          deadline,
        ])
      );

      await testProposal(
        [uniswapV2Pair.address, uniswapV2Router02.address],
        values,
        callData,
        "Proposal #3: Remove liquidity"
      );
    });
  });

  describe("pausing", async () => {
    let wallet: any;

    beforeEach(async () => {
      const [, , , multiSig] = await ethers.getSigners();
      wallet = multiSig;
    });

    describe("spvol token", async () => {
      beforeEach(async () => {
        await proposePause(spVol, wallet.address);
      });

      it("should pause SpendieVol", async () => {
        await spVol.connect(wallet).pause();
        expect(await spVol.paused()).to.be.true;
      });

      it("should emit an event when SpendieVol is paused", async () => {
        await expect(spVol.connect(wallet).pause()).to.emit(spVol, "Paused");
      });

      it("should emit an event when SpendieVol is unpaused", async () => {
        await spVol.connect(wallet).pause();
        await expect(spVol.connect(wallet).unpause()).to.emit(
          spVol,
          "Unpaused"
        );
      });

      it("should revert when sending paused SpendieVol", async () => {
        await spVol.connect(wallet).pause();

        const [deployer, tokenOwner] = await ethers.getSigners();

        await expect(
          spVol
            .connect(deployer)
            .transfer(tokenOwner.address, ethers.utils.parseUnits("1", 8))
        ).to.be.revertedWith("ERC20Pausable: token transfer while paused");
      });
    });

    describe("spgov token", async () => {
      beforeEach(async () => {
        await proposePause(spGov, wallet.address);
      });

      it("should pause SpendieGov", async () => {
        await spGov.connect(wallet).pause();
        expect(await spGov.paused()).to.be.true;
      });

      it("should emit an event when SpendieGov is paused", async () => {
        await expect(spGov.connect(wallet).pause()).to.emit(spGov, "Paused");
      });

      it("should emit an event when SpendieGov is unpaused", async () => {
        await spGov.connect(wallet).pause();
        await expect(spGov.connect(wallet).unpause()).to.emit(
          spGov,
          "Unpaused"
        );
      });

      it("should revert when sending paused SpendieGov", async () => {
        await spGov.connect(wallet).pause();

        const [deployer, tokenOwner] = await ethers.getSigners();

        const spGovTx = await spGov.connect(deployer);

        await expect(
          spGovTx["safeTransferFrom(address,address,uint256)"](
            deployer.address,
            tokenOwner.address,
            1
          )
        ).to.be.revertedWith(
          "ERC721: transfer caller is not owner nor approved"
        );
      });
    });

    describe("stable token", async () => {
      let spAUD: Contract;

      beforeEach(async () => {
        await proposeSPAUD(SPINITIALSUPPLY);

        const spAUDAddress = await basket.getStableToken(0);

        spAUD = new Contract(
          spAUDAddress,
          (await deployments.getArtifact("StableToken")).abi,
          provider.getSigner()
        );

        await proposePause(spAUD, wallet.address);
      });

      it("should pause a stable token", async () => {
        await spAUD.connect(wallet).pause();

        expect(await spAUD.paused()).to.be.true;
      });

      it("should emit an event when stable token is paused", async () => {
        await expect(spAUD.connect(wallet).pause()).to.emit(spAUD, "Paused");
      });

      it("should emit an event when stable token is unpaused", async () => {
        await spAUD.connect(wallet).pause();
        await expect(spAUD.connect(wallet).unpause()).to.emit(
          spAUD,
          "Unpaused"
        );
      });

      it("should revert when sending paused stable token", async () => {
        await spAUD.connect(wallet).pause();

        const [deployer, tokenOwner] = await ethers.getSigners();

        await expect(
          spAUD
            .connect(deployer)
            .transfer(tokenOwner.address, ethers.utils.parseUnits("1", 8))
        ).to.be.revertedWith("ERC20Pausable: token transfer while paused");
      });
    });
  });

  it("should withdraw ether", async () => {
    const [, , , receiver] = await ethers.getSigners();

    const targetAddresses = [treasury.address];
    const values = [ZERO];
    const callDatas = [
      treasury.interface.encodeFunctionData("withdraw", [
        await receiver.getAddress(),
        ethers.utils.parseEther("1"),
      ]),
    ];
    const description = "Withdraw funds";

    const expectedBalance = (
      await provider.getBalance(receiver.getAddress())
    ).add(ethers.utils.parseEther("1"));

    await testProposal(targetAddresses, values, callDatas, description);

    expect(await provider.getBalance(await receiver.getAddress())).to.be.equal(
      expectedBalance
    );
  });

  it("should not withdraw ether without a proposal", async () => {
    await expect(
      treasury.withdraw(
        await provider.getSigner().getAddress(),
        ethers.utils.parseEther("1")
      )
    ).to.be.revertedWith("Treasury/only-treasury-can-withdraw");
  });
});
