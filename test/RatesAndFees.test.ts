/**
 * A catch-all for testing fee and rate calculations. Aggregators are preset
 * with known values to ensure calculations are predetermined.
 */
import { expect } from "chai";
import { ethers, waffle, network } from "hardhat";
import { networkConfig } from "../config";
import { deployMockContract } from "@ethereum-waffle/mock-contract";

import * as AUDUSDPair from "./utils/pricefeeds/chainlink/rates/AUDUSD";
import * as ETHUSDPair from "./utils/pricefeeds/chainlink/rates/ETHUSD";
import * as AUDETHPair from "./utils/pricefeeds/chainlink/rates/AUDETH";

import * as Issuance from "./utils/issuance";
import * as Slippage from "./utils/slippage";

import AggegatorV3InterfaceABI from "@chainlink/contracts/abi/v0.8/AggregatorV3Interface.json";

/**
 * Set up an AUD/USD aggregator where prices can be easily predicted (I.e.
 * hardcoded);
 */
const setupAUDUSDAggegatorMock = async () => {
  const aggregatorMock = await deployMockContract(
    waffle.provider.getSigner(),
    AggegatorV3InterfaceABI
  );

  await aggregatorMock.mock.latestRoundData.returns(
    "18446744073711562376",
    ethers.utils.parseUnits("0.75", 8),
    "1652173723",
    "1652173723",
    "18446744073711562376"
  );

  await aggregatorMock.mock.decimals.returns(8);

  networkConfig[network.name].chainlink.pairs.AUDUSD = aggregatorMock.address;
};

/**
 * Set up an ETH/USD aggregator where prices can be easily predicted (I.e.
 * hardcoded);
 */
const setupETHUSDAggegatorMock = async () => {
  const aggregatorMock = await deployMockContract(
    waffle.provider.getSigner(),
    AggegatorV3InterfaceABI
  );

  await aggregatorMock.mock.latestRoundData.returns(
    "18446744073711562376",
    ethers.utils.parseUnits("2000", 8),
    "1652173723",
    "1652173723",
    "18446744073711562376"
  );

  await aggregatorMock.mock.decimals.returns(8);

  networkConfig[network.name].chainlink.pairs.ETHUSD = aggregatorMock.address;
};

describe("RatesAndFees", () => {
  describe("Exchange Rates", () => {
    let audUSD: any, ethUSD: any;

    before(async () => {
      // save the deployed pair addresses.
      audUSD = networkConfig[network.name].chainlink.pairs.AUDUSD;
      ethUSD = networkConfig[network.name].chainlink.pairs.ETHUSD;

      await setupAUDUSDAggegatorMock();
      await setupETHUSDAggegatorMock();
    });

    after(async () => {
      // set back to the deployed pair addresses.
      networkConfig[network.name].chainlink.pairs.AUDUSD = audUSD;
      networkConfig[network.name].chainlink.pairs.ETHUSD = ethUSD;
    });

    describe("AUD/USD Pair", () => {
      describe("AUD To USD", () => {
        it("should get a rate of 1 AUD = 0.75 USD", async () => {
          expect(await AUDUSDPair.getAUDToUSDRate()).to.be.equal(
            ethers.utils.parseUnits("0.75", 8)
          );
        });
      });

      describe("USD To AUD", () => {
        it("should get a rate of 1.33 AUD = 1 USD", async () => {
          expect(await AUDUSDPair.getUSDToAUDRate()).to.be.equal(
            ethers.utils.parseUnits("1.33333333", 8)
          );
        });
      });
    });

    describe("ETH/USD Pair", () => {
      describe("ETH To USD", () => {
        it("should get a rate of 1 ETH = 2000 USD", async () => {
          expect(await ETHUSDPair.getETHToUSDRate()).to.be.equal(
            ethers.utils.parseUnits("2000", 8)
          );
        });
      });

      describe("USD To ETH", () => {
        it("should get a rate of 1 USD = 0.0005 ETH", async () => {
          expect(await ETHUSDPair.getUSDToETHRate()).to.be.equal(
            ethers.utils.parseEther("0.0005")
          );
        });
      });
    });

    describe("AUD/ETH Pair", () => {
      describe("AUD To ETH", () => {
        it("should get a rate of 1 AUD = 0.000375 ETH", async () => {
          expect(await AUDETHPair.getAUDToETHRate()).to.be.equal(
            ethers.utils.parseEther("0.000375000000000937")
          );
        });

        it("should get 100 AUD = 0.0375 ETH", async () => {
          const amount = ethers.utils.parseUnits("100", 8);
          expect(await AUDETHPair.getAUDToETHAmount(amount)).to.be.equal(
            ethers.utils.parseEther("0.037500000000093700")
          );
        });
      });

      describe("ETH To AUD", () => {
        it("should get a rate of 1 ETH = 2666.66 AUD", async () => {
          expect(await AUDETHPair.getETHToAUDRate()).to.be.equal(
            ethers.utils.parseUnits("2666.66666666", 8)
          );
        });
      });
    });
  });

  describe("Issuance Fees", async () => {
    it("should calculate a 2% issuance fee on 1 USD", () => {
      expect(
        Issuance.calculateIssuanceFee(ethers.utils.parseUnits("1", 8))
      ).to.be.equal(ethers.utils.parseUnits("0.02", 8));
    });

    it("should calculate the amount plus the  issuance fee on 10 USD", () => {
      expect(
        Issuance.getAmountPlusIssuanceFee(ethers.utils.parseUnits("10", 8))
      ).to.be.equal(ethers.utils.parseUnits("10.2", 8));
    });

    it("should calculate the slippage with a price movement of 1% on 1 USD", () => {
      expect(
        Slippage.calculateSlippage(ethers.utils.parseUnits("1", 8))
      ).to.be.equal(ethers.utils.parseUnits("0.01", 8));
    });

    it("should calculate minimum amount with a 1% slippage on 1 USD", () => {
      expect(
        Slippage.getAmountMinusSlippage(ethers.utils.parseUnits("1", 8))
      ).to.be.equal(ethers.utils.parseUnits("0.99", 8));
    });
  });
});
